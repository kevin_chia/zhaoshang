var agentSdk = {
    token: '',
    host: '',
    //获取验证码
    getCaptcha: function (successcCallback, errorCallback) {
        var _this = this;
        $.ajax({
            type: 'post',
            url: this.host + "/captcha.ajax",
            dataType: 'json',
            success: function (result) {
                console.log(result);
                if (result.state) {
                    _this.token = result.data.token;
                    successcCallback(result.data.base64ImageSrc);
                } else {
                    errorCallback(result.message);
                }
            }
        })
    },
    //代理注册
    sendRegister: function (formData, successcCallback, errorCallback) {
        formData.token = this.token;
        console.log(formData);
        $.ajax({
            type: 'post',
            url: this.host + "/register.ajax",
            data: formData,
            dataType: 'json',
            success: function (result) {
                if (result.state == true) {
                    if (successcCallback === undefined) {
                        alert('注册成功');

                        window.location.reload();
                    } else {
                        successcCallback(result);
                    }
                } else {
                    if (errorCallback === undefined) {
                        alert(result.message);
                    } else {
                        errorCallback(result.message);
                    }
                }
            },
            error: function () {
                if (errorCallback === undefined) {
                    alert("网络请求失败，请稍后再试");
                } else {
                    errorCallback("网络请求失败，请稍后再试")
                }
            }
        })
    },
    //代理登录
    login: function (formData, successcCallback, errorCallback) {
        formData.token = this.token;
        console.log(formData);
        $.ajax({
            type: 'post',
            url: this.host + "/login.ajax",
            data: formData,
            dataType: 'json',
            success: function (result) {
                if (result.state == true) {
                    if (successcCallback === undefined) {
                        window.location.href = result.redirect;
                    } else {
                        successcCallback(result.redirect, result.data);
                    }
                } else {
                    if (errorCallback === undefined) {
                        alert(result.message);
                    } else {
                        errorCallback(result.message);
                    }
                }
            },
            error: function () {
                if (errorCallback === undefined) {
                    alert("网络请求失败，请稍后再试");
                } else {
                    errorCallback("网络请求失败，请稍后再试")
                }
            }
        })
    }
};
//判断浏览器跳转手机站
try {
    var urlhash = window.location.hash;
    if (!urlhash.match("fromapp")) {
        if ((navigator.userAgent.match(/(iPhone|iPod|Android|ios|iPad)/i))) {
            window.location = "wap/index.html"; //填写移动版地址
        }
    }
} catch (err) {
}